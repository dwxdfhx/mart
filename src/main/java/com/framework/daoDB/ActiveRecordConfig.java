package com.framework.daoDB;

import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class ActiveRecordConfig implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        PropKit.use("setting.txt");
        initMysqlsh();
        System.out.println("===========启动 ActiveRecordPlugin================== ");
        System.out.println("===========测试mysql数据库================== ");
        long mysqlsh_userCount = Db.use(THIRDCONSTANT.DBMYSQLSH).queryLong("select count(*) from t_user ");
        System.out.println("手环mysql数据库中,用户数量:" + mysqlsh_userCount);
    }

    private void initMysqlsh() {
        DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrlmysqlsh"), PropKit.get("usermysqlsh"), PropKit.get("passwordmysqlsh").trim());
        druidPlugin.setDriverClass("com.mysql.jdbc.Driver");
        druidPlugin.setValidationQuery("select 1");
        druidPlugin.setInitialSize(10);
        druidPlugin.setMinIdle(10);
        druidPlugin.setMaxActive(100);

        ActiveRecordPlugin arp = new ActiveRecordPlugin(THIRDCONSTANT.DBMYSQLSH, druidPlugin);
        arp.setShowSql(PropKit.getBoolean("showSql", false));
        arp.setDialect(new MysqlDialect());
        arp.setTransactionLevel(4);
        //com.ztesoft.zsmartcity.cc.thirduse.mysqlsh.model._MappingKit.mapping(arp);
        druidPlugin.start();
        arp.start();
    }
}