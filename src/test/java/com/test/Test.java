package com.test;

import com.framework.util.ClassUtil;
import com.framework.util.PropsUtil;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by dw on 2018/01/21
 */
public class Test {
    org.slf4j.Logger logger = LoggerFactory.getLogger(Test.class);

    @org.junit.Test
    public void test1() {
        logger.debug("test2");
        logger.error("test error");
        System.out.println(1);
    }

    @org.junit.Test
    public void testproputil() {

        Properties logprop = PropsUtil.loadProps("log4j.properties");

        String a1 = PropsUtil.getValueBykey(logprop, "log4j.logger.com.test", String.class);
        Double a2 = PropsUtil.getValueBykey(logprop, "doubletest", Double.class);
        Integer a3 = PropsUtil.getValueBykey(logprop, "inttest", Integer.class);
        boolean a4 = PropsUtil.getValueBykey(logprop, "false", Boolean.class);
        System.out.println(a1);
        System.out.println(a2);
        System.out.println(a3);
        System.out.println(a4);
    }

    @org.junit.Test
    public void testclassutil() {
        Set<Class<?>> set = com.xiaoleilu.hutool.util.ClassUtil.scanPackage("com.framework");
        System.out.println(set.size());
        for (Class<?> cla : set) {
            System.out.println(cla.getComponentType());
        }


    }
}
