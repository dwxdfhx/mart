package com.framework.daoDB;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

import javax.sql.DataSource;

/**
 * GeneratorDemo
 */
public class MysqlGenerator {


    public static DataSource getDataSource_c3p0() {
        PropKit.use("setting.txt");
        DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrlmysqlsh"), PropKit.get("usermysqlsh"), PropKit.get("passwordmysqlsh").trim());
        druidPlugin.setDriverClass("com.mysql.jdbc.Driver");
        druidPlugin.setValidationQuery("select 1");


        druidPlugin.start();
        return druidPlugin.getDataSource();
    }

    public static void main(String[] args) {
        // base model 所使用的包名
        String baseModelPackageName = "com.framework.daoDB.model.base";
        // base model 文件保存路径src\main\java\com\dw\datadeal\model\base
        String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/framework/daoDB/model/base";
        System.out.println(baseModelOutputDir);
        // model 所使用的包名 (MappingKit 默认使用的包名)
        String modelPackageName = "com.framework.daoDB.model";
        // model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
        String modelOutputDir = baseModelOutputDir + "/..";

        // 创建生成器
        Generator gernerator = new Generator(getDataSource_c3p0(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
        // 设置数据库方言 com.jfinal.plugin.activerecord.dialect
        gernerator.setDialect(new MysqlDialect());
        // 添加不需要生成的表名
        // gernerator.addExcludedTable("adv");
        // 设置是否在 Model 中生成 dao 对象
        gernerator.setGenerateDaoInModel(true);
        // 设置是否生成字典文件
        // gernerator.setGenerateDataDictionary(true);
        // 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为
        // "User"而非 OscUser
        //gernerator.addExcludedTable("MLOG$_");
        // 生成
        gernerator.generate();
    }
}
